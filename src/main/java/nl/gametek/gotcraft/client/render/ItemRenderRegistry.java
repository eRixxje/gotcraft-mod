package nl.gametek.gotcraft.client.render;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.oredict.OreDictionary;
import nl.gametek.gotcraft.Main;
import nl.gametek.gotcraft.init.ModItems;
import nl.gametek.gotcraft.init.ModTools;
import nl.gametek.gotcraft.interfaces.ILocalizable;


public final class ItemRenderRegistry 
{
	public static void Init()
	{
		register(ModItems.GotcraftiumIngot);
		register(ModItems.CornCob);
		register(ModItems.PopCorn);
				
		// Tools
		register(ModTools.GotcraftiumPickAxe);
		register(ModTools.GotcraftiumSpade);
		register(ModTools.GotcraftiumAxe);
		
		register(ModItems.CornSeeds);
		
		OreDictionary.registerOre("ingotGotCraftium", ModItems.GotcraftiumIngot);
	}
	
	public static void register(ILocalizable item) {
	    Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(
	    		(Item) item, 0, new ModelResourceLocation(Main.MODID + ":" + item.getName(), "inventory"));
	}
}