package nl.gametek.gotcraft.client.render;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.oredict.OreDictionary;
import nl.gametek.gotcraft.Main;
import nl.gametek.gotcraft.init.ModBlocks;
import nl.gametek.gotcraft.interfaces.ILocalizable;

public class BlockRenderRegistry {
	
	public static void Init(){
		register(ModBlocks.GotcraftiumOre);
		
		register(ModBlocks.CornCrop);
		
		OreDictionary.registerOre("oreGotcraftium", ModBlocks.GotcraftiumOre);
	}

	public static void register(ILocalizable block) {
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(
				Item.getItemFromBlock((Block)block), 0, new ModelResourceLocation(Main.MODID + ":" + block.getName(), "inventory"));
	}
}
