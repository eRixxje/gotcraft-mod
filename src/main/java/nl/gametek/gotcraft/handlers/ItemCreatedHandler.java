package nl.gametek.gotcraft.handlers;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemSmeltedEvent;
import nl.gametek.gotcraft.init.ModAchievements;
import nl.gametek.gotcraft.init.ModItems;
import nl.gametek.gotcraft.init.ModTools;

public class ItemCreatedHandler {
	
	@SubscribeEvent
	public void onItemCrafted(ItemCraftedEvent event)
	{
		if (event.crafting.getItem() == ModTools.GotcraftiumPickAxe) 
	    {
			event.player.addStat(ModAchievements.achievementCraftedGayPick, 1);
	    }
	}
	
	@SubscribeEvent
	public void onItemSmelted(ItemSmeltedEvent event)
	{
		if (event.smelting.getItem() == ModItems.PopCorn) 
	    {
			event.player.addStat(ModAchievements.achievementSmeltedPopcorn, 1);
	    }
	}
}
