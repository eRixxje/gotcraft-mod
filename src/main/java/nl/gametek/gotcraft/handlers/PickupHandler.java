package nl.gametek.gotcraft.handlers;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import nl.gametek.gotcraft.init.ModAchievements;
import nl.gametek.gotcraft.init.ModBlocks;

public class PickupHandler
{
	@SubscribeEvent
	public void onItemPickup(EntityItemPickupEvent event)
	{
		if(event.item.getEntityItem().getItem() == Item.getItemFromBlock(ModBlocks.GotcraftiumOre))
		{
			event.entityPlayer.addStat(ModAchievements.achievementFoundGotcraftium, 1);
		}
	}
}
