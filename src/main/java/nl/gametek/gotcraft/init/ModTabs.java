package nl.gametek.gotcraft.init;

import net.minecraft.creativetab.CreativeTabs;

import nl.gametek.gotcraft.gui.GotcraftTab;

public final class ModTabs {
	
	public static CreativeTabs gotcraftTab;

	public static void Init()
	{
		gotcraftTab = new GotcraftTab(CreativeTabs.getNextID(), "gotCraftTab");
	}
}
