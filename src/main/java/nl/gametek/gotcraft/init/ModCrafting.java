package nl.gametek.gotcraft.init;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public final class ModCrafting {

	public static void Init()
	{
		AddRecipes();
		AddSmelting();
	}
	private static void AddRecipes()
	{		
		// Create Gotcraftium Tools
		GameRegistry.addRecipe(new ItemStack(ModTools.GotcraftiumPickAxe),
				new Object[] {"CCC"," S "," S ", 'C', ModItems.GotcraftiumIngot, 'S', Items.stick});
		GameRegistry.addRecipe(new ItemStack(ModTools.GotcraftiumAxe),
				new Object[] {"CC ","CS "," S ", 'C', ModItems.GotcraftiumIngot, 'S', Items.stick});
		GameRegistry.addRecipe(new ItemStack(ModTools.GotcraftiumSpade),
				new Object[] {" C "," S "," S ", 'C', ModItems.GotcraftiumIngot, 'S', Items.stick});
	}

	private static void AddSmelting()
	{
		// Smelt 1 Gotcraftium Ore to 1 Gotcraftium Ingot.
		GameRegistry.addSmelting(
				ModBlocks.GotcraftiumOre, 
				new ItemStack(ModItems.GotcraftiumIngot, 1), 1F);
				
		// Smelt 1 Corn Cob to 8 Popcorn.
		GameRegistry.addSmelting(
				ModItems.CornCob, 
				new ItemStack(ModItems.PopCorn, 8), 1F);

	}
}
