package nl.gametek.gotcraft.init;

import net.minecraft.block.material.Material;
import net.minecraftforge.fml.common.registry.GameRegistry;

import nl.gametek.gotcraft.base.blocks.BlockBase;
import nl.gametek.gotcraft.base.blocks.BlockCropBase;

public final class ModBlocks {
	
	//public static BlockBase QuarriedStone;
	
	public static BlockBase GotcraftiumOre;	
	public static BlockCropBase CornCrop;
	
	public static void Init()
	{
		//GameRegistry.registerBlock(
		//		QuarriedStone = new BlockBase("quarried_stone", Material.rock, 1.5F, 1), 
		//		"quarried_stone");
		
		GameRegistry.registerBlock(
				GotcraftiumOre = new BlockBase("gotcraftium_ore", Material.iron, 3F, 2), 
				"gotcraftium_ore");
		
		GameRegistry.registerBlock(
				CornCrop = new BlockCropBase("block_corncrop"), 
				"block_corncrop");
	}
}