package nl.gametek.gotcraft.init;

import net.minecraftforge.fml.common.registry.GameRegistry;
import nl.gametek.gotcraft.base.items.*;

public final class ModTools {
    public static ItemPickAxeBase 	GotcraftiumPickAxe;
    public static ItemSpadeBase 	GotcraftiumSpade;
    public static ItemAxeBase 		GotcraftiumAxe;
    
    public static void Init()
    {
		GameRegistry.registerItem(
				GotcraftiumPickAxe = new ItemPickAxeBase("gotcraftium_pickaxe", ModMaterials.GotcraftiumMaterial), 
				"gotcraftium_pickaxe");		
		GameRegistry.registerItem(
				GotcraftiumSpade = new ItemSpadeBase("gotcraftium_spade", ModMaterials.GotcraftiumMaterial), 
				"gotcraftium_spade");
		GameRegistry.registerItem(
				GotcraftiumAxe = new ItemAxeBase("gotcraftium_axe", ModMaterials.GotcraftiumMaterial), 
				"gotcraftium_axe");
    }
}
