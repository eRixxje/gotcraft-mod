package nl.gametek.gotcraft.init;

import net.minecraft.stats.Achievement;
import net.minecraftforge.common.AchievementPage;

public final class ModAchievements {

	public static Achievement achievementFoundGotcraftium;
	public static Achievement achievementCraftedGayPick;
	public static Achievement achievementSmeltedPopcorn;

	public static void Init()
	{
		achievementFoundGotcraftium = 
				new Achievement("achievement.gotcraft_foundgotcraftium", "gotcraft_foundgotcraftium", 
						0, 0, ModBlocks.GotcraftiumOre, (Achievement)null);
		achievementCraftedGayPick = 
				new Achievement("achievement.gotcraft_craftedgaypick", "gotcraft_craftedgaypick", 
						2, 1, ModTools.GotcraftiumPickAxe, achievementFoundGotcraftium);			
		achievementSmeltedPopcorn = 
				new Achievement("achievement.gotcraft_smeltedpopcorn", "gotcraft_smeltedpopcorn", 
						0, -2, ModItems.PopCorn, (Achievement)null);
		
		achievementFoundGotcraftium.registerStat();
		achievementCraftedGayPick.registerStat();
		achievementSmeltedPopcorn.registerStat();
		
		AchievementPage.registerAchievementPage(
			    new AchievementPage("Gotcraft", 
			            new Achievement[] {achievementFoundGotcraftium, achievementCraftedGayPick, achievementSmeltedPopcorn}));
	}
}
