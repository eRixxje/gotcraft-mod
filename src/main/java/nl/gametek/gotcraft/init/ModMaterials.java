package nl.gametek.gotcraft.init;

import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;

public final class ModMaterials {

	 public static ToolMaterial GotcraftiumMaterial;
	 
	 public static void Init()
	 {
		 /*
		 2. Harvest Level � level of material that can harvest it (defined as 0 = WOOD/GOLD, 1 = STONE, 2 = IRON, 3 = DIAMOND) 
		 3. Max Uses � number of uses this material allows(e.g.: WOOD = 59, STONE = 131, IRON = 250, DIAMOND = 1561, GOLD = 32) 
		 4. Efficiency � strength when used on blocks (e.g.: WOOD = 2.0F, STONE = 4.0F, IRON = 4.0F, DIAMOND = 8.0F, GOLD = 12.0F).  
		 5. Damage � additional damage factor done to entities (e.g.: WOOD = 0.0F, STONE = 1.0F, IRON = 2.0F, DIAMOND = 3.0F, GOLD = 0.0F).  This will be added to default damage done by whatever weapon, e.g.: sword = 4.0F.  Remember, this needs to be double the number of hearts of damage, so to do 10 hearts of damage would need at least 20.  
		 6. Enchantability � natural enchantability factor of the material (e.g.: WOOD = 15, STONE = 5, IRON = 14, DIAMOND = 10, GOLD = 22).  A higher value means it is more enchantable.  
		 */
		 
		 GotcraftiumMaterial = 
				 EnumHelper.addToolMaterial("GotcraftiumMaterial", 
		    	    		3, 3000, 15.0F, 6.0F, 50);
	 }
}
