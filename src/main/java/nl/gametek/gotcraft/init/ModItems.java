package nl.gametek.gotcraft.init;

import net.minecraftforge.fml.common.registry.GameRegistry;

import nl.gametek.gotcraft.base.items.ItemBase;
import nl.gametek.gotcraft.base.items.ItemFoodBase;
import nl.gametek.gotcraft.base.items.ItemSeedsBase;

public final class ModItems {
		
	public static ItemBase 		GotcraftiumIngot;
	
	public static ItemBase 		CornCob;
    public static ItemFoodBase 	PopCorn;
    public static ItemSeedsBase CornSeeds;
    
	public static void Init()
	{
		GameRegistry.registerItem(
				GotcraftiumIngot = new ItemBase("gotcraftium_ingot"), 
				"gotcraftium_ingot");
		
		GameRegistry.registerItem(
				CornCob = new ItemBase("corn_cob"), 
				"corn_cob");
		
		GameRegistry.registerItem(
				PopCorn = new ItemFoodBase("popcorn", 1, 0.6F), 
				"popcorn");
		
		GameRegistry.registerItem(
				CornSeeds = new ItemSeedsBase(ModBlocks.CornCrop, null, "corn_seeds"), 
				"corn_seeds");
	}
}