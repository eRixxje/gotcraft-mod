package nl.gametek.gotcraft.base.blocks;

import net.minecraft.block.BlockCrops;
import net.minecraft.item.Item;
import nl.gametek.gotcraft.Main;
import nl.gametek.gotcraft.init.ModItems;
import nl.gametek.gotcraft.interfaces.ILocalizable;

public class BlockCropBase extends BlockCrops implements ILocalizable {
	protected String name;
	
	public String getName()
	{
		return name;
	}
		
	public BlockCropBase(String UnlocalizedName)
	{
		name = UnlocalizedName;
		
		// Set full unlocalized name for lang ref.
		setUnlocalizedName(Main.MODID + "_" + name);
		
		// Dont Add to Gotcraft Tab.
		//setCreativeTab(Main.gotcraftTab);
	}
	
	
	
	@Override
	protected Item getSeed()
	{
		return ModItems.CornSeeds;
	}
	
	@Override
	protected Item getCrop()
	{
		return ModItems.CornCob;
	}
}
