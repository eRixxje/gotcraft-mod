package nl.gametek.gotcraft.base.blocks;

import net.minecraft.block.material.Material;
import nl.gametek.gotcraft.Main;
import nl.gametek.gotcraft.init.ModTabs;
import nl.gametek.gotcraft.interfaces.ILocalizable;

public class BlockBase extends net.minecraft.block.Block implements ILocalizable {
	protected String name;
	
	public String getName()
	{
		return name;
	}
		
	public BlockBase(String UnlocalizedName, Material material, float Hardness, int level)
	{
		super(material);
		
		setHardness(Hardness);
		setHarvestLevel("pickaxe", level);
		
		name = UnlocalizedName;
		
		// Set full unlocalized name for lang ref.
		setUnlocalizedName(Main.MODID + "_" + name);
		
		// Add to Gotcraft Tab.
		setCreativeTab(ModTabs.gotcraftTab);
	}
}