package nl.gametek.gotcraft.base.items;

import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemArmor;
import nl.gametek.gotcraft.Main;
import nl.gametek.gotcraft.init.ModTabs;
import nl.gametek.gotcraft.interfaces.ILocalizable;

public class ItemArmorBase extends ItemArmor implements ILocalizable {
	
	protected String name;
	
	public String getName()
	{
		return name;
	}
	
	public ItemArmorBase(String unlocalizedName, ArmorMaterial material, int renderIndex, EntityEquipmentSlot i) {
        super(material, renderIndex, i);
		
		name = unlocalizedName;
		
		// Set full unlocalized name for lang ref.
		setUnlocalizedName(Main.MODID + "_" + name);
		
		// Add to Gotcraft Tab.
		setCreativeTab(ModTabs.gotcraftTab);
    }
}