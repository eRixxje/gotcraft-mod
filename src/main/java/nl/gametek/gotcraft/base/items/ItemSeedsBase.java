package nl.gametek.gotcraft.base.items;

import net.minecraft.block.Block;
import nl.gametek.gotcraft.Main;
import nl.gametek.gotcraft.init.ModTabs;
import nl.gametek.gotcraft.interfaces.ILocalizable;

public class ItemSeedsBase extends net.minecraft.item.ItemSeeds implements ILocalizable {
	
	protected String name;
	
	public String getName()
	{
		return name;
	}
	
	public ItemSeedsBase(Block crops, Block soil, String UnlocalizedName) 
	{
		super(crops, soil);
		
		name = UnlocalizedName;
		
		// Set full unlocalized name for lang ref.
		setUnlocalizedName(Main.MODID + "_" + name);
						
		// Add to Gotcraft Tab.
		setCreativeTab(ModTabs.gotcraftTab);
	}
}
