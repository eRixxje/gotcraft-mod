package nl.gametek.gotcraft.base.items;

import net.minecraft.item.ItemHoe;
import nl.gametek.gotcraft.Main;
import nl.gametek.gotcraft.init.ModTabs;
import nl.gametek.gotcraft.interfaces.ILocalizable;

public class ItemHoeBase extends ItemHoe implements ILocalizable {
	protected String name;
	
	public String getName()
	{
		return name;
	}
	
	public ItemHoeBase(String UnlocalizedName, ToolMaterial material)
	{
		super(material);
		
		name = UnlocalizedName;
		
		// Set full unlocalized name for lang ref.
		setUnlocalizedName(Main.MODID + "_" + name);
		
		// Add to Gotcraft Tab.
		setCreativeTab(ModTabs.gotcraftTab);
	}
}
