package nl.gametek.gotcraft.base.items;

import net.minecraft.item.ItemFood;
import nl.gametek.gotcraft.Main;
import nl.gametek.gotcraft.init.ModTabs;
import nl.gametek.gotcraft.interfaces.ILocalizable;

public class ItemFoodBase extends ItemFood implements ILocalizable {
	private final String name;
	
	public String getName()
	{
		return name;
	}
	
	public ItemFoodBase(String UnlocalizedName, int Amount, float Saturation)
	{
		super(Amount, Saturation, false);
		
		name = UnlocalizedName;
		
		setUnlocalizedName(Main.MODID + "_" + name);
		
		// Add to Gotcraft Tab.
		setCreativeTab(ModTabs.gotcraftTab);
	}
}