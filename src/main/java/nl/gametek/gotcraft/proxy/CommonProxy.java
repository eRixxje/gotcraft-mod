package nl.gametek.gotcraft.proxy;

import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import nl.gametek.gotcraft.WorldGen;
import nl.gametek.gotcraft.handlers.ItemCreatedHandler;
import nl.gametek.gotcraft.handlers.PickupHandler;
import nl.gametek.gotcraft.init.ModAchievements;
import nl.gametek.gotcraft.init.ModBlocks;
import nl.gametek.gotcraft.init.ModCrafting;
import nl.gametek.gotcraft.init.ModItems;
import nl.gametek.gotcraft.init.ModMaterials;
import nl.gametek.gotcraft.init.ModTabs;
import nl.gametek.gotcraft.init.ModTools;

public class CommonProxy {

    public void preInit(FMLPreInitializationEvent e) {
    	ModTabs.Init();
    	
    	ModMaterials.Init();   	
    	ModBlocks.Init();
        ModItems.Init();       
        ModTools.Init();
        
        ModAchievements.Init();
    }

    public void init(FMLInitializationEvent e) {
    	ModCrafting.Init();
    	
    	GameRegistry.registerWorldGenerator(new WorldGen(), 0);
    	
    	MinecraftForge.addGrassSeed(new ItemStack(ModItems.CornSeeds), 5);
    	
    	MinecraftForge.EVENT_BUS.register(new PickupHandler());
    	MinecraftForge.EVENT_BUS.register(new ItemCreatedHandler());
    	//MinecraftForge.EVENT_BUS.register(new SmeltedHandler());
    }

    public void postInit(FMLPostInitializationEvent e) {

    }
}